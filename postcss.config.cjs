const tailwindcss = require('tailwindcss');
const autoprefixer = require('autoprefixer');

const config = {
	content: ["./src/**/*.{html,js,ts,svelte"],
	plugins: [
		//Some plugins, like tailwindcss/nesting, need to run before Tailwind,
		tailwindcss(),
		//But others, like autoprefixer, need to run after,
		autoprefixer
	]
};

module.exports = config;
